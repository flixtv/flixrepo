# <a href="repository.BrunoFlixRepo.zip">DOWNLOAD DO REPOSITÓRIO</a>

Instruções para a adição no gestor:


<p align="left">
  <ul>
    <li>Ir para o Kodi gestor de ficheiros.</li>
    <li>Clicar em "Adicionar fonte"</li>
    <li>O endereço para a fonte é <code>https://tinyurl.com/BrunoFlixRepo</code> (Dar o nome de "BrunoFlixRepo").</li>
    <li>Ir para "Addons"</li>
    <li>Em Addons, instalar de um ficheiro zip. Quando perguntar pela localização, selecionar "repository.BrunoFlixRepo", e instalar <a href="repository.BrunoFlixRepo.zip">repository.BrunoFlixRepo.zip</a>.</li>
    -
    <li>Repositório Instalado!</li>
    
</ul>

                                      
                                       

</p>

